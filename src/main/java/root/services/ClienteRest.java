
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.dao.ClienteJpaController;
import root.dao.exceptions.NonexistentEntityException;
import root.persistence.entities.Cliente;

@Path("clientes")
public class ClienteRest {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("apk_clientes");
    EntityManager em;
    ClienteJpaController dao = new ClienteJpaController();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {
        /*em = emf.createEntityManager();
        List<Cliente> lista = em.createNamedQuery("Cliente.findAll").getResultList();
        return Response.ok(200).entity(lista).build();*/
        
        List<Cliente> lista = dao.findClienteEntities();
        
        return Response.ok(200).entity(lista).build();
        
    }

    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar) {

        /*em = emf.createEntityManager();
        Cliente cliente = em.find(Cliente.class, idbuscar);
        return Response.ok(200).entity(cliente).build();*/

        Cliente cliente = dao.findCliente(idbuscar);
        return Response.ok(200).entity(cliente).build();
        
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response nuevo(Cliente clienteNuevo) throws Exception {

       /* em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(clienteNuevo);
        em.getTransaction().commit();
        return Response.ok("Cliente Guardado").build();*/

        Cliente cliente = new Cliente();
        dao.create(clienteNuevo);
        
        return Response.ok("Cliente Guardado").build();
        
        
      
        
    }

    @PUT
    public Response actualizar(Cliente clienteUpdate) throws Exception {
        String resultado = null;

        /*em = emf.createEntityManager();
        em.getTransaction().begin();
        clienteUpdate = em.merge(clienteUpdate);
        em.getTransaction().commit();
        return Response.ok(clienteUpdate).build();*/

        Cliente cliente = new Cliente();
        dao.edit(clienteUpdate);
        
        return Response.ok(clienteUpdate).build();
        
    }

    @DELETE
    @Path("/{iddelete}")
    @Produces({MediaType .TEXT_PLAIN,MediaType  .APPLICATION_JSON})
    public Response eliminaId(@PathParam("iddelete") String iddelete) throws NonexistentEntityException {
        
        /*em = emf.createEntityManager();
        em.getTransaction().begin();
        Cliente clienteEliminar = em.getReference(Cliente.class, iddelete);
        em.remove(clienteEliminar);
        em.getTransaction().commit();
        return Response.ok("Cliente Eliminado").build();*/

        Cliente cliente = new Cliente();
        dao.destroy(iddelete);
        
        return Response.ok("Cliente Eliminado").build();
        
    }

}
