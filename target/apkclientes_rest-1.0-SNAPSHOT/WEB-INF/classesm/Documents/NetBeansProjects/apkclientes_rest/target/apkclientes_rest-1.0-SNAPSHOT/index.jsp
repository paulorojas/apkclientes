<%-- 
    Document   : index
    Created on : 05-may-2020, 18:11:57
    Author     : LiL WOLF
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>apkclientes_rest</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <h1>APK Clientes Rest</h1>

         <div id="table">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th style="width: 11%; text-align: center" scope="col">Request</th>
                    <th style="width: 5%; text-align: center" scope="col">Verbo</th>
                    <th style="width: 37%; text-align: center"scope="col">URL</th>
                    <th style="width: 43%; text-align: center" scope="col">Descripción</th>
                </tr>
            </thead>
           
            <tbody>
                <tr>
                    <th style="text-align: center; padding-top: 34px"  scope="row">Listar Clientes</th>
                    <td style="text-align: center; padding-top: 28px" id="get">GET</td>
                    <td id="url" style="padding-top: 35px">http://DESKTOP-EQ7PPE4:8080/apkclientes_rest-1.0-SNAPSHOT/api/clientes</td>
                    <td>Este recurso se utiliza para llamar la lista de clientes que se encuentra almacenada en nuestra Base de Datos, para ello hacemos uso del método GET, y escribimos la URL correspondiente.</td>
                </tr>
                <tr>
                    <th style="text-align: center; padding-top: 34px" scope="row">Buscar Cliente</th>
                    <td style="text-align: center; padding-top: 28px" id="get">GET</td>
                    <td id="url" style="padding-top: 35px">http://DESKTOP-EQ7PPE4:8080/apkclientes_rest-1.0-SNAPSHOT/api/clientes/19651258-1</td>
                    <td>Es utilizado para buscar un cliente en nuestra Base de Datos, esto se hace mediante el ID (PK) que le asignamos en la misma BD, usamos el método GET, y la URL que se muestra donde “19651258-1” corresponde al ID.</td>
                </tr>
                <tr>
                    <th style="text-align: center; padding-top: 34px" scope="row">Crear Cliente</th>
                    <td style="text-align: center; padding-top: 28px" id="post">POST</td>
                    <td id="url" style="padding-top: 35px">http://DESKTOP-EQ7PPE4:8080/apkclientes_rest-1.0-SNAPSHOT/api/clientes</td>
                    <td>Como el mismo nombre del recurso lo indica es usado para añadir un nuevo cliente a nuestra BD, lo hacemos por medio del método POST, con la URL que se muestra, y escribimos los datos del cliente en el body mediante un formato JSON.</td>
                </tr>
                <tr>
                    <th style="text-align: left; padding-top: 34px" scope="row">Actualizar Cliente</th>
                    <td style="text-align: center; padding-top: 28px" id="put">PUT</td>
                    <td id="url" style="padding-top: 35px">http://DESKTOP-EQ7PPE4:8080/apkclientes_rest-1.0-SNAPSHOT/api/clientes</td>
                    <td>Este recurso es utilizado para cambiar los datos de un determinado campo (excepto el ID) de un cliente que se encuentra registrado en nuestra BD, lo hacemos usando el método PUT, y con la URL que se indica, al igual que para crear un cliente lo hacemos a través del body con JSON.</td>
                </tr>
                <tr>
                    <th style="text-align: center; padding-top: 34px" scope="row">Eliminar Cliente</th>
                    <td style="text-align: center; padding-top: 28px" id="delete">DELETE</td>
                    <td id="url" style="padding-top: 35px">http://DESKTOP-EQ7PPE4:8080/apkclientes_rest-1.0-SNAPSHOT/api/clientes/19651258-1</td>
                    <td>Utilizamos este recurso cuando deseamos eliminar un cliente que se encuentra en nuestra BD, se ocupa el método DELETE, y la URL que se muestra, donde “19651258-1” corresponde al ID del cliente que deseamos retirar de nuestra BD.</td>
                </tr>
            </tbody>
        </table>

         </div>


    </body>
</html>
